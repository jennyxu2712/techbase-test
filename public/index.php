<?php

require '../vendor/autoload.php';
defined('BASE_PATH') || define('BASE_PATH', dirname(__DIR__));

use Libs\Request;
use Libs\Config;

$config = Config::getInstance();

try {
    $request = new Request();
    $request->process();
} catch (\Exception $e) {
    if ($config->get('debug')) {
        dd($e);
    }
    // log o day
}

function dd($content = '') {
    print_r('<pre>');
    print_r($content);
    print_r('</pre>');
    die;
}
