<?php

namespace App\Controllers;

class StaffController {

    public function index() {
        $limit = isset($_GET['limit']) ? $_GET['limit'] : NULL;
        $staffService = new \App\Services\Staff;
        $staffs = $staffService->getStaffs($limit);
        echo json_encode($staffs);
        exit;
    }
}
