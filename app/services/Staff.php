<?php

namespace App\Services;

use Libs\Databases\Connection;

class Staff {

    const LIMIT_MAX = 1500;

    public function getStaffs($limit = 100) {
        $db = (Connection::getInstance())->getConnection();
        $limit = $this->_getLimit($limit);

        $sql = 'SELECT * FROM staffs LIMIT ' . $limit . ';';
        $statement = $db->prepare($sql);
        $statement->execute();
        $res = $statement->fetchAll(\PDO::FETCH_ASSOC);
        if (count($res)) {
            $projectOfStaffs = $this->_getProjectsOfStaffs(array_column($res, 'id'));
            foreach ($res as &$e) {
                $e['joins'] = isset($projectOfStaffs[$e['id']]) ? $projectOfStaffs[$e['id']] : [];
            }
        }
        return $res;
    }

    private function _getLimit($limit) {
        if (is_null($limit) || $limit > self::LIMIT_MAX) {
            $limit = self::LIMIT_MAX;
        }
        return $limit;
    }

    private function _getProjectsOfStaffs($staffIds) {
        if (empty($staffIds)) {
            return [];
        }
        $db = (Connection::getInstance())->getConnection();
        $sql = sprintf('
            SELECT ps.staff_id, p.id as project_id, p.name as project_name, d.id as department_id, d.name as department_name, s.name as manager FROM projects_staffs ps
            JOIN projects p ON ps.project_id = p.id
            JOIN departments d ON p.department_id = d.id
            JOIN staffs s ON s.id = d.manager
            WHERE ps.staff_id IN (%s)
        ', implode(',', array_fill(0, count($staffIds), '?')));

        $statement = $db->prepare($sql);
        $statement->execute($staffIds);
        $res = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $lst = [];
        foreach ($res as $e) {
            if (!isset($e['staff_id'])) {
                $lst[$e['staff_id']] = [
                    $e['department_id'] => [
                        'department_id' => $e['department_id'],
                        'department_name' => $e['department_name'],
                        'manager' => $e['manger'],
                        'projects' => []
                    ]
                ];
            }
            $lst[$e['staff_id']][$e['department_id']]['projects'][] = [
                'project_id' => $e['project_id'],
                'project_name' => $e['project_name']
            ];
        }
        return $lst;
    }
}
