<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Services\Staff as StaffService;

class StaffTest extends TestCase {

    protected $_staffSerive;

    protected function setUp() {
        defined('BASE_PATH') || define('BASE_PATH', dirname(__DIR__) . '/..');
        $this->_staffSerive = new StaffService;

    }

    public function testGetStaffsReturnArray() {
        $staffs = $this->_staffSerive->getStaffs();
        $this->assertTrue(is_array($staffs));
    }

    public function testMaxLimitIs1500() {
        $this->assertEquals(1500, $this->callMethod($this->_staffSerive, '_getLimit', [1800]));
    }

    public function testLimitReturn1500WhenInputNull() {
        $this->assertEquals(1500, $this->callMethod($this->_staffSerive, '_getLimit', [NULL]));
    }

    public function testGetProjectsOfStaffsReturnArray() {
        $res = $this->callMethod($this->_staffSerive, '_getProjectsOfStaffs', [[1]]);
        $this->assertTrue(is_array($res));
    }

    public function testGetProjectsOfStaffsReturnEmptyWhenNoStaffIds() {
        $res = $this->callMethod($this->_staffSerive, '_getProjectsOfStaffs', [[]]);
        $this->assertEmpty($res);
    }

    public function testGetStaffOfProjects() {
        $staffIds = [1, 3];
        $expectedRes = [
            '1' => [
                '1' => [
                    'projects' => [
                        ['project_id' => '1', 'project_name' => 'Dự án quản lý nhân sự']
                    ]
                ],
                '2' => [
                    'projects' => [
                        ['project_id' => '3', 'project_name' => 'Dự án xác thực bằng gương mặt'],
                    ]
                ]
            ],
            '3' => [
                '1' => [
                    'projects' => [
                        ['project_id' => '2', 'project_name' => 'Dự án booking']
                    ]
                ]
            ]
        ];
        $this->assertEquals(json_encode($this->callMethod($this->_staffSerive, '_getProjectsOfStaffs', [$staffIds])), json_encode($expectedRes));
    }

    public function testGetStaffsExistJoinsAttributeWhenStaffIsMemberOfProjects() {
        $limit = 1;
        $res = $this->_staffSerive->getStaffs($limit);
        $this->assertArrayHasKey('joins', $res[0]);
    }

    public function callMethod($object, $method, $parameters = []) {
        try {
            $className = get_class($object);
            $reflection = new \ReflectionClass($className);
        } catch (\ReflectionException $e) {
           throw new \Exception($e->getMessage());
        }

        $method = $reflection->getMethod($method);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
