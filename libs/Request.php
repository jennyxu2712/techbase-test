<?php

namespace Libs;

class Request {

    private $_controller = 'index';
    private $_action = 'index';

    public function process() {
        $this->_getPath();
        $controllerClass = $this->_createController();
        $this->_callAction($controllerClass);
    }

    private function _getPath() {
        if (isset($_SERVER['REQUEST_URI'])) {
            $requestUri = $_SERVER['REQUEST_URI'];
            $requestUri = trim(preg_replace('/\/index.php/', '', $requestUri));
            if ($requestUri) {
                $parts = explode('/', $requestUri);
                $countParts = count($parts);
                if ($countParts > 1) {
                    $this->_controller = $parts[1];
                    if ($countParts > 2) {
                        $this->_action = $parts[2];
                    }
                }
            }
        }
    }

    private function _createController() {
        $className = '\\App\\Controllers\\' . ucfirst($this->_controller) . 'Controller';
        if (class_exists(ucfirst($className))) {
            return new $className;
        }
        throw new \Exception('Controller khong ton tai');
    }

    // khong ho tro path
    private function _callAction($controllerClass) {
        $action = $this->_action;
        if (is_callable([$controllerClass, $action])) {
            $controllerClass->$action();
        }
        throw new \Exception('Action khong ton tai');
    }
}
