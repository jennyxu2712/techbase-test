<?php

namespace Libs;

class Config {
    private static $_instance = NULL;
    protected $_configs = [];

    private function __construct($environment = 'development') {
        $this->_configs = require BASE_PATH . '/config/' . $environment . '.php';
    }

    public static function getInstance() {
        if (!self::$_instance) {
            self::$_instance = new Config;
        }
        return self::$_instance;
    }

    public function get($key) {
        $parts = explode('.', $key);
        $res = $this->_configs;
        foreach ($parts as $p) {
            if (!$res) break;
            $res = $this->_getkey($res, $p);
        }
        return $res;
    }

    private function _getKey($data, $key) {
        if (isset($data[$key])) {
            return $data[$key];
        }
        return NULL;
    }
}
