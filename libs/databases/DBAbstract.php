<?php

namespace Libs\Databases;

use Libs\Config;

class DBAbstract {

    protected $_connection;
    protected $_dbConfig;

    public function __construct() {
        $config = Config::getInstance();
        $this->_dbConfig = $config->get('db');
        $this->_connect();
    }

    public function getConnection() {
        return $this->_connection;
    }
}
