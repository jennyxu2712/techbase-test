<?php

namespace Libs\Databases;

use Libs\Config;

class Connection {

    private static $_instance;

    private $_connection;

    private function __construct() {
        $config = Config::getInstance();
        $driver = $config->get('db.driver');
        if ($driver) {
            $className = '\\Libs\\Databases\\Connections\\' . ucfirst($driver) . 'Connection';
            if (class_exists($className)) {
                $this->_connection = new $className;
                return;
            }
            throw new \Exception('Khong ho tro driver');
        }
        throw new \Exception('Chua khai bao driver');
    }

    public static function getInstance() {
        if (!self::$_instance) {
            self::$_instance = new Connection;
        }
        return self::$_instance;
    }

    public function getConnection() {
        return $this->_connection->getConnection();
    }
}
