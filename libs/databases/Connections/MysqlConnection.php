<?php

namespace Libs\Databases\Connections;

use Libs\Databases\DBAbstract;

class MysqlConnection extends DBAbstract implements ConnectionInterface {

    protected $_connection;

    public function _connect() {
        $username = $this->_dbConfig['username'];
        $password = $this->_dbConfig['password'];
        $charset = $this->_dbConfig['charset'];
        $host = $this->_dbConfig['host'];
        $dbName = $this->_dbConfig['database_name'];
        $dsn = sprintf('mysql:host=%s;dbname=%s;charset=%s', $host, $dbName, $charset);
        $this->_connection = new \PDO($dsn, $username, $password);
    }

    public function beginTransaction() {
        $this->_connection->beginTransaction();
    }

    public function commit() {
        $this->_connection->commit();
    }

    public function rollback() {
        $this->_connection->rollback();
    }
}
