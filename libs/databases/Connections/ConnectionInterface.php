<?php

namespace Libs\Databases\Connections;

interface ConnectionInterface
{
    function _connect();

    function beginTransaction();

    function commit();

    function rollback();
}
