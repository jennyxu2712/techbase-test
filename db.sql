CREATE TABLE IF NOT EXISTS staffs
(
    id int primary key auto_increment,
    name varchar(100),
    created_at int,
    updated_at int
);

INSERT INTO staffs (id, name, created_at, updated_at) VALUES
(1, 'Nguyễn Văn A', UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(2, 'Nguyễn Van B', UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(3, 'Nguyễn Văn C', UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(4, 'Trần Văn A', UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(5, 'Trần Văn B', UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(6, 'Trần Văn C', UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(7, 'Manager 1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(8, 'Manager 2', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());

CREATE TABLE IF NOT EXISTS departments
(
    id int primary key auto_increment,
    name varchar(100),
    manager int NOT NULL,
    created_at int,
    updated_at int,
    INDEX (manager)
);

INSERT INTO departments (id, name, manager, created_at, updated_at) VALUES (1, 'Phòng kỹ thuật', 7, UNIX_TIMESTAMP(), UNIX_TIMESTAMP()), (2, 'Phòng R&D', 8, UNIX_TIMESTAMP(), UNIX_TIMESTAMP());

CREATE TABLE IF NOT EXISTS projects
(
    id int primary key auto_increment,
    name varchar(100),
    department_id int not null,
    created_at int,
    updated_at int,
    INDEX (department_id)
);

INSERT INTO projects (id, name, department_id, created_at, updated_at) VALUES
(1, 'Dự án quản lý nhân sự', 1, UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(2, 'Dự án booking', 1, UNIX_TIMESTAMP(), UNIX_TIMESTAMP()),
(3, 'Dự án xác thực bằng gương mặt', 2, UNIX_TIMESTAMP(), UNIX_TIMESTAMP());

CREATE TABLE IF NOT EXISTS projects_staffs
(
    project_id int,
    staff_id int,
    INDEX (project_id, staff_id),
    INDEX (project_id),
    INDEX (staff_id)
);

INSERT INTO projects_staffs (project_id, staff_id) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 5),
(3, 6),
(3, 1),
(3, 2);
