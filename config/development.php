<?php

return [
    'debug' => 1,
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'database_name' => 'techbase_test',
        'charset' => 'utf8',
    ]
];
